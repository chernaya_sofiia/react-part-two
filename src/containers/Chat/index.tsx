import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ChatHeader from '../../components/Chat/ChatHeader';
import MessageList from '../../components/Chat/MessageList';
import MessageInput from '../../components/Chat/MessageInput';
import Preloader from '../../components/Preloader';
import EditMessage from '../../components/Chat/EditMessage';

import {
  loadMessages,
  sendMessage,
  likeMessage,
  deleteMessage,
  toggleEditMessage,
  editMessage 
} from '../../store/Chat/actions';
  
import { IMessage } from '../../interfaces/Message';
import { IChatState } from '../../interfaces/ChatState';

import './index.scss';

type ChatProps = {
  messages?: IMessage[]
  partisipantsNumber: number
  isLoading: boolean
  inEditMessage: IMessage | undefined
  toggleEditMessage: (toggle: boolean) => void
  loadMessages: () => void
  sendMessage: (text: string) => void
  likeMessage: (id: string) => void
  deleteMessage: (id: string) => void
  editMessage: (text: string) => void
}

const Chat: React.FC<ChatProps> = ({
  messages = [],
  partisipantsNumber,
  isLoading,
  inEditMessage,
  toggleEditMessage: toggleEditAction,
  loadMessages: loadMessageAction,
  sendMessage: sendMessageAction,
  likeMessage: likeMessageAction,
  deleteMessage: deleteMessageAction,
  editMessage: editMessageAction
}): JSX.Element => {
  useEffect(() => {
    if (isLoading) {
      loadMessageAction();
    }
  }, [isLoading, loadMessageAction]);

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'ArrowUp') {
      toggleEditAction(true);
    }
  };

  const lastMessage = () => {
    if (messages.length) {
      return messages ? messages[messages.length - 1].createdAt : '';
    }
    return '';
  };

  return (
    isLoading
      ? <Preloader />
      : (
        <div tabIndex={0} className='chat-container' onKeyDown={handleKeyPress}>
          <ChatHeader
            name='Ozark chat'
            partisipantsNumber={partisipantsNumber}
            messages={messages.length}
            lastMessage={lastMessage()}
          />
          <MessageList
            messages={messages}
            likeMessage={likeMessageAction}
            deleteMessage={deleteMessageAction}
            toggleEdit={toggleEditAction}
          />
          <MessageInput
            sendMessage={sendMessageAction}
          />
          {inEditMessage
            ? (
              <EditMessage
                message={inEditMessage}
                apply={editMessageAction}
                toggle={toggleEditAction}
              />
            )
            : null}
        </div>
      )
  );
};

Chat.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any),
  partisipantsNumber: PropTypes.number.isRequired,
  isLoading: PropTypes.bool.isRequired,
  inEditMessage: PropTypes.any,
  toggleEditMessage: PropTypes.func.isRequired,
  loadMessages: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired
};

Chat.defaultProps = {
  messages: [],
  partisipantsNumber: 0,
  inEditMessage: undefined
};

const mapStateToProps = (state: IChatState) => ({
  messages: state.messages,
  partisipantsNumber: state.partisipantsNumber,
  isLoading: state.isLoading,
  inEditMessage: state.inEditMessage
});

const mapDispatchToProps = {
  toggleEditMessage,
  loadMessages,
  sendMessage,
  likeMessage,
  deleteMessage,
  editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);

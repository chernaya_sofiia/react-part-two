export interface IMessageCallbacks {
    likeMessage: (id: string) => void
    deleteMessage: (id: string) => void
    toggleEdit: (toggle: boolean) => void
}

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { IMessage } from '../../../interfaces/Message';
import { IEditCallbacks } from '../../../interfaces/EditCallbacks';

import './index.scss';

type EditProps = { message: IMessage } & IEditCallbacks;

const EditMessage = ({ message, toggle, apply }: EditProps) => {
  const [text, setText] = useState(message.text);

  const handleSave = (toSend: string) => {
    apply(toSend);
    toggle(false);
  };

  return (
    <div className='edit-message-modal' tabIndex={-1} role="dialog">
      <div className="modal-content">
        <div className="modal-body">
          <textarea
            autoFocus
            className='edit-message-input'
            value={text}
            placeholder="Write a message"
            onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
              setText(ev.target.value);
            }}
          />
        </div>
        <div className='modal-footer'>
          <button
            type='button'
            className='edit-message-button'
            onClick={() => handleSave(text)}
          >
            Save
            </button>
          <button
            type='button'
            className='edit-message-close'
            data-dismiss='modal'
            onClick={() => toggle(false)}
          >
            Close
            </button>
        </div>
      </div>
    </div>
  );
};

EditMessage.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
  toggle: PropTypes.func.isRequired,
  apply: PropTypes.func.isRequired
};

export default EditMessage;

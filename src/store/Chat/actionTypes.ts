import { IMessage } from '../../interfaces/Message';

export const SET_MESSAGES = 'SET_MESSAGES';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';

export interface ISetMessagesAction {
    type: typeof SET_MESSAGES
    messages: IMessage[]
}

export interface ISendMessageAction {
    type: typeof SEND_MESSAGE
    payload: IMessage
}

export interface ISetEditMessageAction {
    type: typeof EDIT_MESSAGE
    payload: IMessage | undefined
}

export type IActionTypes = ISetMessagesAction
    | ISendMessageAction
    | ISetEditMessageAction;

import { IProfile } from '../Profile'
import { IMessage } from '../Message'

export interface IChatState {
    messages: IMessage[]
    partisipantsNumber: number
    isLoading: boolean
    profile: IProfile
    inEditMessage?: IMessage
}

import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import chatReducer from './store/Chat/reducer';

const composedEnhancers = composeWithDevTools(
  applyMiddleware(ReduxThunk)
);

const store = createStore(chatReducer, composedEnhancers);

export default store;

export interface IMessage {
    id: string
    text: string
    user: string
    avatar?: string
    userId: string
    editedAt: string
    createdAt: string
    canEdit: boolean
    likesCount: number
    myMessage: boolean
}

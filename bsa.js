import Chat from './src/containers/Chat';

import rootReducer from './src/rootReducer';

export default {
    Chat,
    rootReducer,
};

import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import {
  SET_MESSAGES,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  ISetMessagesAction,
  ISendMessageAction,
  ISetEditMessageAction
} from './actionTypes';


import { IMessage } from '../../interfaces/Message';
import { IFetchedMessages } from '../../interfaces/FetchedMessages';
import { IChatState } from '../../interfaces/ChatState';

export const sendMessageAction = (newMessage: IMessage): ISendMessageAction => ({
  type: SEND_MESSAGE,
  payload: newMessage
});

export const setMessagesAction = (messages: IMessage[]): ISetMessagesAction => ({
  type: SET_MESSAGES,
  messages
});

export const setEditMessage = (message: IMessage | undefined): ISetEditMessageAction => ({
  type: EDIT_MESSAGE,
  payload: message
});

const updateMock = (fetchedMessages: IFetchedMessages[], currentUserId: string, partisipantsNumber: number): IMessage[] => {
  if (!fetchedMessages.length) {
    return [];
  }
  return fetchedMessages.map(message => ({
    ...message,
    partisipantsNumber: partisipantsNumber,
    likesCount: 0,
    canEdit: false,
    myMessage: currentUserId === message.userId
  }));
};

export const toggleEditMessage = (toggle: boolean): ThunkAction<void, IChatState, unknown, Action> =>
  (dispatch, getState) => {
    if (toggle) {
      const { messages, profile: { userId } } = getState();
      const userMessages = messages.filter(message => message.userId === userId);
      if (userMessages.length) {
        const lastMessage = userMessages[userMessages.length - 1];
        dispatch(setEditMessage(lastMessage));
        return;
      }
    }
    dispatch(setEditMessage(undefined));
  };

export const loadMessages = (): ThunkAction<void, IChatState, unknown, Action> =>
  async (dispatch, getState) => {
    try {
      const { profile: { userId } } = getState();

      const url = 'https://edikdolynskyi.github.io/react_sources/messages.json';
      const messages = await (await fetch(url, { method: "GET" })).json() as IFetchedMessages[];

      const fetchedMessages = messages.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
      const usersMap = new Map();
      messages.forEach(message => {
        usersMap.set(message.user, typeof usersMap.get(message.user) !== 'undefined' ? (1 + usersMap.get(message.user)) : 1);
      });
      const partisipantsNumber = usersMap.size;
      const updatedMock = updateMock(fetchedMessages, userId, partisipantsNumber);
      dispatch(setMessagesAction(updatedMock));
    } catch (err) {
      console.warn(err);
    }
  };

export const sendMessage = (text: string): ThunkAction<void, IChatState, unknown, Action> =>
  (dispatch, getState) => {
    const { messages, profile: { userId, user } } = getState();
    const updatedMessages = messages.map(message => ({ ...message, canEdit: false }));
    dispatch(setMessagesAction(updatedMessages));

    const updatedMock: IMessage = {
      id: Math.random().toString(),
      text,
      user,
      userId,
      editedAt: '',
      createdAt: new Date().toJSON(),
      canEdit: true,
      likesCount: 0,
      myMessage: true
    };
    dispatch(sendMessageAction(updatedMock));
  };

export const likeMessage = (id: string): ThunkAction<void, IChatState, unknown, Action> =>
  (dispatch, getState) => {
    const { messages } = getState();
    const updatedMessages = messages.map(message => {
      if (message.id === id) {
        const diff = message.likesCount === 1 ? -1 : 1;
        const newLikesCount = message.likesCount + diff;
        return {
          ...message,
          likesCount: newLikesCount
        };
      }
      return message;
    });
    dispatch(setMessagesAction(updatedMessages));
  };

export const deleteMessage = (id: string): ThunkAction<void, IChatState, unknown, Action> =>
  (dispatch, getState) => {
    const { messages, profile: { userId } } = getState();
    const withoutMessage = messages.filter(message => message.id !== id);
    const userMessages = withoutMessage.filter(message => message.userId === userId);
    if (userMessages.length) {
      const lastMessage = userMessages[userMessages.length - 1];
      const updated = withoutMessage.map(message => {
        if (message.id === lastMessage.id) {
          return {
            ...message,
            canEdit: true
          };
        }
        return message;
      });
      dispatch(setMessagesAction(updated));
    } else {
      dispatch(setMessagesAction(withoutMessage));
    }
  };

export const editMessage = (text: string): ThunkAction<void, IChatState, unknown, Action> =>
  (dispatch, getState) => {
    const { messages, inEditMessage } = getState();
    if (inEditMessage) {
      const updatedMessages = messages.map(message => {
        if (message.id === inEditMessage.id) {
          const editedAt = new Date().toJSON();
          return {
            ...inEditMessage,
            editedAt,
            text
          };
        }
        return message;
      });
      dispatch(setMessagesAction(updatedMessages));
    }
  };

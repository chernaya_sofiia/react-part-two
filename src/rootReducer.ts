import { createStore } from 'redux';
import chatReducer from './store/Chat/reducer';

export default createStore(chatReducer);

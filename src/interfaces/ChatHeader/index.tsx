export interface IChatHeader {
    name: string,
    partisipantsNumber: number,
    messages: number,
    lastMessage: string
}

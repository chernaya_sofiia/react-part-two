import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './index.scss';

type MessageInputProps = {
  sendMessage: (message: string) => void
}

const MessageInput = ({ sendMessage }: MessageInputProps): JSX.Element => {
  const [toSend, setToSend] = useState<string>('');

  const handleSend = (text: string) => {
    if (!text.trim().length) {
      return;
    }
    sendMessage(text);
    setToSend('');
  };

  return (
    <div className='message-input'>
      <textarea
        className='message-input-text'
        value={toSend}
        placeholder="Write a message"
        onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
          setToSend(ev.target.value);
        }}
      />
      <button
        type="button"
        className='message-input-button'
        onClick={() => handleSend(toSend)}
      >
        Send
      </button>
    </div>
  );
};

MessageInput.propTypes = {
  sendMessage: PropTypes.func.isRequired,
};

export default MessageInput;

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './index.scss';

import { IChatHeader } from '../../../interfaces/ChatHeader';

const ChatHeader = ({
  name,
  partisipantsNumber,
  messages,
  lastMessage
}: IChatHeader): JSX.Element => {
  const messageDate = moment(lastMessage).format('DD.MM.YYYY HH:mm');

  return (
    <div className='chat-header'>
      <div className='chat-info'>
        <span className='header-title'>{name}</span>
        <span className='header-users-count'>{4} participants</span>
        <span className='header-messages-count'>{messages} messages</span>
      </div>
      <div className='header-last-message-date'>
        <span>last message at {messageDate}</span>
      </div>
    </div>
  );
};

ChatHeader.propTypes = {
  name: PropTypes.string.isRequired,
  partisipantsNumber: PropTypes.number.isRequired,
  messages: PropTypes.number.isRequired,
  lastMessage: PropTypes.string.isRequired
};

export default ChatHeader;

export interface IEditCallbacks {
    toggle: (toggle: boolean) => void
    apply: (text: string) => void
}

import { 
    SET_MESSAGES, 
    SEND_MESSAGE, 
    EDIT_MESSAGE, 
    IActionTypes
} from './actionTypes';
import { IChatState } from '../../interfaces/ChatState';

const initialState: IChatState = {
    partisipantsNumber: 0,
    messages: [],
    isLoading: true,
    profile: { user: 'Sophia', userId: 'userId' }
};

export default (state = initialState, action: IActionTypes) => {
    switch (action.type) {
        case SET_MESSAGES:
            return {
                ...state,
                messages: action.messages,
                isLoading: false
            };
        case SEND_MESSAGE: {
            return {
                ...state,
                messages: [...(state.messages || []), action.payload]
            };
        }
        case EDIT_MESSAGE: {
            return {
                ...state,
                inEditMessage: action.payload
            };
        }
        default: {
            return state;
        }
    }
};

import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import './index.scss';

import { IMessage } from '../../../interfaces/Message';
import { IMessageCallbacks } from '../../../interfaces/MessageCallbacks';

type MessageProps = IMessage & IMessageCallbacks;

const OwnMessage: React.FC<MessageProps> = ({
    id,
    text,
    createdAt,
    editedAt,
    user,
    canEdit,
    likesCount,
    deleteMessage,
    toggleEdit
}): JSX.Element => {
    const creationTime = moment(createdAt).format('HH:mm');

    const editButton = canEdit
        ? (
            <button
                type="button"
                className='message-edit'
                onClick={() => toggleEdit(true)}
            >
                &#9881;
            </button>
        )
        : null;

    return (
        <div className='own-message'>
            <div className='message-body'>
                <div className='message-header'>
                    <span className='message-user-name'>{user}</span>
                    <div>
                        {editButton}
                        <button
                            type='button'
                            className='message-delete'
                            onClick={() => deleteMessage(id)}
                        >
                            X
                        </button>
                    </div>
                </div>
                <div className='message-text'>
                    {text}
                </div>
                <div className='message-like-time'>
                    <div className='like-wrap'>
                        <button
                            type='button'
                            className='message-like'
                        >
                            &#128077;
                </button>
                        <div className='like-count'>{likesCount}</div>
                    </div>
                    <div className='message-time'>
                        {editedAt.length ? 'edited • ' : null}
                        {creationTime}
                    </div>
                </div>
            </div>
        </div>
    );
};

OwnMessage.defaultProps = {
    avatar: ''
};

OwnMessage.propTypes = {
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    avatar: PropTypes.string,
    createdAt: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    canEdit: PropTypes.bool.isRequired,
    likesCount: PropTypes.number.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    toggleEdit: PropTypes.func.isRequired,
    myMessage: PropTypes.bool.isRequired
};

export default OwnMessage;

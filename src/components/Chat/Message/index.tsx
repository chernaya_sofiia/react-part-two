import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Avatar from '../Avatar';

import './index.scss';

import { IMessage } from '../../../interfaces/Message';
import { IMessageCallbacks } from '../../../interfaces/MessageCallbacks';

type MessageProps = IMessage & IMessageCallbacks;

const Message: React.FC<MessageProps> = ({
  id,
  text,
  avatar,
  createdAt,
  editedAt,
  user,
  likesCount,
  likeMessage
}): JSX.Element => {
  const creationTime = moment(createdAt).format('HH:mm');

  return (
    <div className='message'>
      {avatar ? <Avatar src={avatar} /> : null}
      <div className='message-body'>
        <div className='message-header'>
          <span className='message-user-name'>{user}</span>
        </div>
        <div className='message-text'>
          {text}
        </div>
        <div className='message-like-time'>
          <div className='like-wrap'>
            <button
              type='button'
              className='message-like'
              onClick={() => likeMessage(id)}
            >
              &#128077;
                </button>
            <div className='like-count'>{likesCount}</div>
          </div>
          <div className='message-time'>
            {editedAt.length ? 'edited • ' : null}
            {creationTime}
          </div>
        </div>
      </div>
    </div>
  );
};

Message.defaultProps = {
  avatar: ''
};

Message.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  createdAt: PropTypes.string.isRequired,
  editedAt: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  canEdit: PropTypes.bool.isRequired,
  likesCount: PropTypes.number.isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  toggleEdit: PropTypes.func.isRequired,
  myMessage: PropTypes.bool.isRequired
};

export default Message;
